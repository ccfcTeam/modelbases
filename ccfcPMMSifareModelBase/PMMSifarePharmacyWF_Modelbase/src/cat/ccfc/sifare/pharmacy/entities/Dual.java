package cat.ccfc.sifare.pharmacy.entities;

public class Dual  implements java.io.Serializable {

    public Dual() {
        super();
    }

    private String dummy;

    public Dual(String dummy) {
        this.dummy = dummy;
    }


    public String getDummy() {
        return this.dummy;
    }
    
    public void setDummy(String dummy) {
        this.dummy = dummy;
    }


}


