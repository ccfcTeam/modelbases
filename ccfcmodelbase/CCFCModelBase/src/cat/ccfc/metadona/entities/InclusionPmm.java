package cat.ccfc.metadona.entities;
// Generated Jun 16, 2015 2:00:31 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * InclusionPmm generated by hbm2java
 */
public class InclusionPmm  implements java.io.Serializable {


     private BigDecimal inclusionId;
     private Prescripcion prescripcion;
     private Lookup lookupByTipoMotivoAltaFk;
     private Lookup lookupByMotivoInclusionFk;
     private String pacienteNombre;
     private String pacienteApellido1;
     private String pacienteApellido2;
     private String pacienteNif;
     private String pacienteCip;
     private Date pacienteFecNac;
     private String pacienteFkPaisNac;
     private String pacienteFkProvinciaNac;
     private String pacienteFkPaisResi;
     private String pacienteFkCaResi;
     private String pacienteFkProvResi;
     private String pacienteFkComResi;
     private String pacienteFkMuniResi;
     private String motivosInclusionOtros;
     private String centroNombre;
     private String centroCodigo;
     private String centroTlf;
     private String centroFax;
     private Date prescriptorFecDerivacion;
     private String farmaciaNombre;
     private String farmaciaId;
     private String farmaciaPoblacion;
     private String farmaciaNumColegiado;
     private Date farmaciaFecInclusion;
     private String textHistEvolucion;
     private String textHistActitud;
     private String textHistSituacion;
     private String textObjetivos;
     private String textMedicacion;
     private String pacienteDireccion;
     private boolean prescriptorPrivado;
     private BigDecimal edadInicioConsumo;
     private Date fechaFinalizacion;
     private String observaciones;
     private String estado;
     private BigDecimal centroFk;
     private String checkinStatus;
     private BigDecimal checkinStep;
     private BigDecimal avisoBajaConfirmado;
     private BigDecimal signatureId;
     private BigDecimal signatureCheckoutId;
     private Set recetas = new HashSet(0);
     private Set ordenMedicas = new HashSet(0);
     private Consentimiento consentimiento;
     private Set citas = new HashSet(0);
     private Set cierres = new HashSet(0);
     private Set incidencias = new HashSet(0);
     private Set personas = new HashSet(0);

     //This field not persisted in DB. Used only for display in UI
     private BigDecimal age;
     //This field not persisted in DB. Used only for display in UI
     private BigDecimal yearsEnrolled;

    public BigDecimal getYearsEnrolled() {
        Calendar yearEnrolled = Calendar.getInstance();
        yearEnrolled.setTime(getPrescriptorFecDerivacion());
        Calendar currentDate = Calendar.getInstance();
        int years = (currentDate.get(Calendar.YEAR) )- yearEnrolled.get(Calendar.YEAR);
        return new BigDecimal (years);
    }

    public void setYearsEnrolled(BigDecimal yearsEnrolled) {
        this.yearsEnrolled = yearsEnrolled;
    }

     
    public BigDecimal getAge() {
        Calendar dob = Calendar.getInstance();
        dob.setTime(getPacienteFecNac());
        Calendar currentDate = Calendar.getInstance();
        int age = (currentDate.get(Calendar.YEAR) )- dob.get(Calendar.YEAR);
        return new BigDecimal (age);
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public InclusionPmm() {
    }

	
    public InclusionPmm(BigDecimal inclusionId, Lookup lookupByMotivoInclusionFk, String pacienteNombre, String pacienteApellido1, String pacienteApellido2, String pacienteNif, String pacienteCip, Date pacienteFecNac, String farmaciaId, boolean prescriptorPrivado, BigDecimal edadInicioConsumo) {
        this.inclusionId = inclusionId;
        this.lookupByMotivoInclusionFk = lookupByMotivoInclusionFk;
        this.pacienteNombre = pacienteNombre;
        this.pacienteApellido1 = pacienteApellido1;
        this.pacienteApellido2 = pacienteApellido2;
        this.pacienteNif = pacienteNif;
        this.pacienteCip = pacienteCip;
        this.pacienteFecNac = pacienteFecNac;
        this.farmaciaId = farmaciaId;
        this.prescriptorPrivado = prescriptorPrivado;
        this.edadInicioConsumo = edadInicioConsumo;
    }
    public InclusionPmm(BigDecimal inclusionId, Prescripcion prescripcion, Lookup lookupByTipoMotivoAltaFk, Lookup lookupByMotivoInclusionFk, String pacienteNombre, String pacienteApellido1, String pacienteApellido2, String pacienteNif, String pacienteCip, Date pacienteFecNac, String pacienteFkPaisNac, String pacienteFkProvinciaNac, String pacienteFkPaisResi, String pacienteFkCaResi, String pacienteFkProvResi, String pacienteFkComResi, String pacienteFkMuniResi, String motivosInclusionOtros, String centroNombre, String centroCodigo, String centroTlf, String centroFax, Date prescriptorFecDerivacion, String farmaciaNombre, String farmaciaId, String farmaciaPoblacion, String farmaciaNumColegiado, Date farmaciaFecInclusion, String textHistEvolucion, String textHistActitud, String textHistSituacion, String textObjetivos, String textMedicacion, String pacienteDireccion, boolean prescriptorPrivado, BigDecimal edadInicioConsumo, Date fechaFinalizacion, String observaciones, String estado, BigDecimal centroFk, String checkinStatus, BigDecimal checkinStep, BigDecimal avisoBajaConfirmado, BigDecimal signatureId, BigDecimal signatureCheckoutId, Set recetas, Set ordenMedicas, Consentimiento consentimiento, Set citas, Set cierres, Set incidencias, Set personas) {
       this.inclusionId = inclusionId;
       this.prescripcion = prescripcion;
       this.lookupByTipoMotivoAltaFk = lookupByTipoMotivoAltaFk;
       this.lookupByMotivoInclusionFk = lookupByMotivoInclusionFk;
       this.pacienteNombre = pacienteNombre;
       this.pacienteApellido1 = pacienteApellido1;
       this.pacienteApellido2 = pacienteApellido2;
       this.pacienteNif = pacienteNif;
       this.pacienteCip = pacienteCip;
       this.pacienteFecNac = pacienteFecNac;
       this.pacienteFkPaisNac = pacienteFkPaisNac;
       this.pacienteFkProvinciaNac = pacienteFkProvinciaNac;
       this.pacienteFkPaisResi = pacienteFkPaisResi;
       this.pacienteFkCaResi = pacienteFkCaResi;
       this.pacienteFkProvResi = pacienteFkProvResi;
       this.pacienteFkComResi = pacienteFkComResi;
       this.pacienteFkMuniResi = pacienteFkMuniResi;
       this.motivosInclusionOtros = motivosInclusionOtros;
       this.centroNombre = centroNombre;
       this.centroCodigo = centroCodigo;
       this.centroTlf = centroTlf;
       this.centroFax = centroFax;
       this.prescriptorFecDerivacion = prescriptorFecDerivacion;
       this.farmaciaNombre = farmaciaNombre;
       this.farmaciaId = farmaciaId;
       this.farmaciaPoblacion = farmaciaPoblacion;
       this.farmaciaNumColegiado = farmaciaNumColegiado;
       this.farmaciaFecInclusion = farmaciaFecInclusion;
       this.textHistEvolucion = textHistEvolucion;
       this.textHistActitud = textHistActitud;
       this.textHistSituacion = textHistSituacion;
       this.textObjetivos = textObjetivos;
       this.textMedicacion = textMedicacion;
       this.pacienteDireccion = pacienteDireccion;
       this.prescriptorPrivado = prescriptorPrivado;
       this.edadInicioConsumo = edadInicioConsumo;
       this.fechaFinalizacion = fechaFinalizacion;
       this.observaciones = observaciones;
       this.estado = estado;
       this.centroFk = centroFk;
       this.checkinStatus = checkinStatus;
       this.checkinStep = checkinStep;
       this.avisoBajaConfirmado = avisoBajaConfirmado;
       this.signatureId = signatureId;
       this.signatureCheckoutId = signatureCheckoutId;
       this.recetas = recetas;
       this.ordenMedicas = ordenMedicas;
       this.consentimiento = consentimiento;
       this.citas = citas;
       this.cierres = cierres;
       this.incidencias = incidencias;
       this.personas = personas;
    }
   
    public BigDecimal getInclusionId() {
        return this.inclusionId;
    }
    
    public void setInclusionId(BigDecimal inclusionId) {
        this.inclusionId = inclusionId;
    }
    public Prescripcion getPrescripcion() {
        return this.prescripcion;
    }
    
    public void setPrescripcion(Prescripcion prescripcion) {
        this.prescripcion = prescripcion;
    }
    public Lookup getLookupByTipoMotivoAltaFk() {
        return this.lookupByTipoMotivoAltaFk;
    }
    
    public void setLookupByTipoMotivoAltaFk(Lookup lookupByTipoMotivoAltaFk) {
        this.lookupByTipoMotivoAltaFk = lookupByTipoMotivoAltaFk;
    }
    public Lookup getLookupByMotivoInclusionFk() {
        return this.lookupByMotivoInclusionFk;
    }
    
    public void setLookupByMotivoInclusionFk(Lookup lookupByMotivoInclusionFk) {
        this.lookupByMotivoInclusionFk = lookupByMotivoInclusionFk;
    }
    public String getPacienteNombre() {
        return this.pacienteNombre;
    }
    
    public void setPacienteNombre(String pacienteNombre) {
        this.pacienteNombre = pacienteNombre;
    }
    public String getPacienteApellido1() {
        return this.pacienteApellido1;
    }
    
    public void setPacienteApellido1(String pacienteApellido1) {
        this.pacienteApellido1 = pacienteApellido1;
    }
    public String getPacienteApellido2() {
        return this.pacienteApellido2;
    }
    
    public void setPacienteApellido2(String pacienteApellido2) {
        this.pacienteApellido2 = pacienteApellido2;
    }
    public String getPacienteNif() {
        return this.pacienteNif;
    }
    
    public void setPacienteNif(String pacienteNif) {
        this.pacienteNif = pacienteNif;
    }
    public String getPacienteCip() {
        return this.pacienteCip;
    }
    
    public void setPacienteCip(String pacienteCip) {
        this.pacienteCip = pacienteCip;
    }
    public Date getPacienteFecNac() {
        return this.pacienteFecNac;
    }
    
    public void setPacienteFecNac(Date pacienteFecNac) {
        this.pacienteFecNac = pacienteFecNac;
    }
    public String getPacienteFkPaisNac() {
        return this.pacienteFkPaisNac;
    }
    
    public void setPacienteFkPaisNac(String pacienteFkPaisNac) {
        this.pacienteFkPaisNac = pacienteFkPaisNac;
    }
    public String getPacienteFkProvinciaNac() {
        return this.pacienteFkProvinciaNac;
    }
    
    public void setPacienteFkProvinciaNac(String pacienteFkProvinciaNac) {
        this.pacienteFkProvinciaNac = pacienteFkProvinciaNac;
    }
    public String getPacienteFkPaisResi() {
        return this.pacienteFkPaisResi;
    }
    
    public void setPacienteFkPaisResi(String pacienteFkPaisResi) {
        this.pacienteFkPaisResi = pacienteFkPaisResi;
    }
    public String getPacienteFkCaResi() {
        return this.pacienteFkCaResi;
    }
    
    public void setPacienteFkCaResi(String pacienteFkCaResi) {
        this.pacienteFkCaResi = pacienteFkCaResi;
    }
    public String getPacienteFkProvResi() {
        return this.pacienteFkProvResi;
    }
    
    public void setPacienteFkProvResi(String pacienteFkProvResi) {
        this.pacienteFkProvResi = pacienteFkProvResi;
    }
    public String getPacienteFkComResi() {
        return this.pacienteFkComResi;
    }
    
    public void setPacienteFkComResi(String pacienteFkComResi) {
        this.pacienteFkComResi = pacienteFkComResi;
    }
    public String getPacienteFkMuniResi() {
        return this.pacienteFkMuniResi;
    }
    
    public void setPacienteFkMuniResi(String pacienteFkMuniResi) {
        this.pacienteFkMuniResi = pacienteFkMuniResi;
    }
    public String getMotivosInclusionOtros() {
        return this.motivosInclusionOtros;
    }
    
    public void setMotivosInclusionOtros(String motivosInclusionOtros) {
        this.motivosInclusionOtros = motivosInclusionOtros;
    }
    public String getCentroNombre() {
        return this.centroNombre;
    }
    
    public void setCentroNombre(String centroNombre) {
        this.centroNombre = centroNombre;
    }
    public String getCentroCodigo() {
        return this.centroCodigo;
    }
    
    public void setCentroCodigo(String centroCodigo) {
        this.centroCodigo = centroCodigo;
    }
    public String getCentroTlf() {
        return this.centroTlf;
    }
    
    public void setCentroTlf(String centroTlf) {
        this.centroTlf = centroTlf;
    }
    public String getCentroFax() {
        return this.centroFax;
    }
    
    public void setCentroFax(String centroFax) {
        this.centroFax = centroFax;
    }
    public Date getPrescriptorFecDerivacion() {
        return this.prescriptorFecDerivacion;
    }
    
    public void setPrescriptorFecDerivacion(Date prescriptorFecDerivacion) {
        this.prescriptorFecDerivacion = prescriptorFecDerivacion;
    }
    public String getFarmaciaNombre() {
        return this.farmaciaNombre;
    }
    
    public void setFarmaciaNombre(String farmaciaNombre) {
        this.farmaciaNombre = farmaciaNombre;
    }
    public String getFarmaciaId() {
        return this.farmaciaId;
    }
    
    public void setFarmaciaId(String farmaciaId) {
        this.farmaciaId = farmaciaId;
    }
    public String getFarmaciaPoblacion() {
        return this.farmaciaPoblacion;
    }
    
    public void setFarmaciaPoblacion(String farmaciaPoblacion) {
        this.farmaciaPoblacion = farmaciaPoblacion;
    }
    public String getFarmaciaNumColegiado() {
        return this.farmaciaNumColegiado;
    }
    
    public void setFarmaciaNumColegiado(String farmaciaNumColegiado) {
        this.farmaciaNumColegiado = farmaciaNumColegiado;
    }
    public Date getFarmaciaFecInclusion() {
        return this.farmaciaFecInclusion;
    }
    
    public void setFarmaciaFecInclusion(Date farmaciaFecInclusion) {
        this.farmaciaFecInclusion = farmaciaFecInclusion;
    }
    public String getTextHistEvolucion() {
        return this.textHistEvolucion;
    }
    
    public void setTextHistEvolucion(String textHistEvolucion) {
        this.textHistEvolucion = textHistEvolucion;
    }
    public String getTextHistActitud() {
        return this.textHistActitud;
    }
    
    public void setTextHistActitud(String textHistActitud) {
        this.textHistActitud = textHistActitud;
    }
    public String getTextHistSituacion() {
        return this.textHistSituacion;
    }
    
    public void setTextHistSituacion(String textHistSituacion) {
        this.textHistSituacion = textHistSituacion;
    }
    public String getTextObjetivos() {
        return this.textObjetivos;
    }
    
    public void setTextObjetivos(String textObjetivos) {
        this.textObjetivos = textObjetivos;
    }
    public String getTextMedicacion() {
        return this.textMedicacion;
    }
    
    public void setTextMedicacion(String textMedicacion) {
        this.textMedicacion = textMedicacion;
    }
    public String getPacienteDireccion() {
        return this.pacienteDireccion;
    }
    
    public void setPacienteDireccion(String pacienteDireccion) {
        this.pacienteDireccion = pacienteDireccion;
    }
    public boolean isPrescriptorPrivado() {
        return this.prescriptorPrivado;
    }
    
    public void setPrescriptorPrivado(boolean prescriptorPrivado) {
        this.prescriptorPrivado = prescriptorPrivado;
    }
    public BigDecimal getEdadInicioConsumo() {
        return this.edadInicioConsumo;
    }
    
    public void setEdadInicioConsumo(BigDecimal edadInicioConsumo) {
        this.edadInicioConsumo = edadInicioConsumo;
    }
    public Date getFechaFinalizacion() {
        return this.fechaFinalizacion;
    }
    
    public void setFechaFinalizacion(Date fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }
    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public BigDecimal getCentroFk() {
        return this.centroFk;
    }
    
    public void setCentroFk(BigDecimal centroFk) {
        this.centroFk = centroFk;
    }
    public String getCheckinStatus() {
        return this.checkinStatus;
    }
    
    public void setCheckinStatus(String checkinStatus) {
        this.checkinStatus = checkinStatus;
    }
    public BigDecimal getCheckinStep() {
        return this.checkinStep;
    }
    
    public void setCheckinStep(BigDecimal checkinStep) {
        this.checkinStep = checkinStep;
    }
    public BigDecimal getAvisoBajaConfirmado() {
        return this.avisoBajaConfirmado;
    }
    
    public void setAvisoBajaConfirmado(BigDecimal avisoBajaConfirmado) {
        this.avisoBajaConfirmado = avisoBajaConfirmado;
    }
    public BigDecimal getSignatureId() {
        return this.signatureId;
    }
    
    public void setSignatureId(BigDecimal signatureId) {
        this.signatureId = signatureId;
    }
    public BigDecimal getSignatureCheckoutId() {
        return this.signatureCheckoutId;
    }
    
    public void setSignatureCheckoutId(BigDecimal signatureCheckoutId) {
        this.signatureCheckoutId = signatureCheckoutId;
    }
    public Set getRecetas() {
        return this.recetas;
    }
    
    public void setRecetas(Set recetas) {
        this.recetas = recetas;
    }
    public Set getOrdenMedicas() {
        return this.ordenMedicas;
    }
    
    public void setOrdenMedicas(Set ordenMedicas) {
        this.ordenMedicas = ordenMedicas;
    }
    public Consentimiento getConsentimiento() {
        return this.consentimiento;
    }
    
    public void setConsentimiento(Consentimiento consentimiento) {
        this.consentimiento = consentimiento;
    }
    public Set getCitas() {
        return this.citas;
    }
    
    public void setCitas(Set citas) {
        this.citas = citas;
    }
    public Set getCierres() {
        return this.cierres;
    }
    
    public void setCierres(Set cierres) {
        this.cierres = cierres;
    }
    public Set getIncidencias() {
        return this.incidencias;
    }
    
    public void setIncidencias(Set incidencias) {
        this.incidencias = incidencias;
    }
    public Set getPersonas() {
        return this.personas;
    }
    
    public void setPersonas(Set personas) {
        this.personas = personas;
    }




}


