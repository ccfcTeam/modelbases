package cat.ccfc.metadona.entities;

import java.io.Serializable;
import java.util.Date;

public class CcfcFarmacias   implements Serializable{
    public CcfcFarmacias() {
        super();
    }
    
    private String idFarmaciaUp;
    private String fkColegio;
    private String cif;
    private String razonSocial;
    private Date fechaAlta;
    private String idCfg;
    private String tipoOf;


    public void setIdFarmaciaUp(String idFarmaciaUp) {
        this.idFarmaciaUp = idFarmaciaUp;
    }

    public String getIdFarmaciaUp() {
        return idFarmaciaUp;
    }

    public void setFkColegio(String fkColegio) {
        this.fkColegio = fkColegio;
    }

    public String getFkColegio() {
        return fkColegio;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCif() {
        return cif;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setIdCfg(String idCfg) {
        this.idCfg = idCfg;
    }

    public String getIdCfg() {
        return idCfg;
    }

    public void setTipoOf(String tipoOf) {
        this.tipoOf = tipoOf;
    }

    public String getTipoOf() {
        return tipoOf;
    }
}
