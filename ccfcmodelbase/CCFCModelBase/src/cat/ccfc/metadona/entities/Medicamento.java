package cat.ccfc.metadona.entities;
// Generated Jan 2, 2015 4:24:46 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;

/**
 * Medicamento generated by hbm2java
 */
public class Medicamento  implements java.io.Serializable {


     private BigDecimal idMedicamento;
     private Lookup lookup;
     private String codigo;
     private String descripcion;
     private BigDecimal mg;

    public Medicamento() {
    }

	
    public Medicamento(BigDecimal idMedicamento, Lookup lookup, String codigo) {
        this.idMedicamento = idMedicamento;
        this.lookup = lookup;
        this.codigo = codigo;
    }
    public Medicamento(BigDecimal idMedicamento, Lookup lookup, String codigo, String descripcion, BigDecimal mg) {
       this.idMedicamento = idMedicamento;
       this.lookup = lookup;
       this.codigo = codigo;
       this.descripcion = descripcion;
       this.mg = mg;
    }
   
    public BigDecimal getIdMedicamento() {
        return this.idMedicamento;
    }
    
    public void setIdMedicamento(BigDecimal idMedicamento) {
        this.idMedicamento = idMedicamento;
    }
    public Lookup getLookup() {
        return this.lookup;
    }
    
    public void setLookup(Lookup lookup) {
        this.lookup = lookup;
    }
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public BigDecimal getMg() {
        return this.mg;
    }
    
    public void setMg(BigDecimal mg) {
        this.mg = mg;
    }




}


