package cat.ccfc.metadona.entities;
// Generated Mar 10, 2015 7:30:16 PM by Hibernate Tools 4.3.1



/**
 * CcfcFarmaciolaHist generated by hbm2java
 */
public class CcfcFarmaciolaHist  implements java.io.Serializable {


     private CcfcFarmaciolaHistId id;

    public CcfcFarmaciolaHist() {
    }

    public CcfcFarmaciolaHist(CcfcFarmaciolaHistId id) {
       this.id = id;
    }
   
    public CcfcFarmaciolaHistId getId() {
        return this.id;
    }
    
    public void setId(CcfcFarmaciolaHistId id) {
        this.id = id;
    }




}


