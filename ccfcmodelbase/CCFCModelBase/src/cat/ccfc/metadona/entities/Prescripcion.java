package cat.ccfc.metadona.entities;
// Generated Feb 11, 2015 12:10:26 PM by Hibernate Tools 4.3.1


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Prescripcion generated by hbm2java
 */
public class Prescripcion  implements java.io.Serializable {


     private BigDecimal prescripcionId;
     private Prescriptor prescriptor;
     private InclusionPmm inclusionPmm;
     private Lookup lookupByTipoPrescripcionFk;
     private Lookup lookupByTipoFrecPautaFk;
     private String via;
     private BigDecimal dosis;
     private String posologia;
     private BigDecimal tipoFormaFk;
     private Date fechaInicial;
     private Date fechaFinal;
     private String textoPrescripcion;
     private Timestamp fechaCarga;
     private String estado;
     private BigDecimal tipoFk;
     private String observaciones;
     private Set recetas = new HashSet(0);
     private Set ordenMedicas = new HashSet(0);
     private Set inclusionPmms = new HashSet(0);
     private Set citas = new HashSet(0);
     private Set incidencias = new HashSet(0);

    public Prescripcion() {
    }

	
    public Prescripcion(BigDecimal prescripcionId, Prescriptor prescriptor, InclusionPmm inclusionPmm, Lookup lookupByTipoFrecPautaFk, String via, BigDecimal dosis, String posologia, BigDecimal tipoFormaFk, Date fechaInicial, Date fechaFinal, Timestamp fechaCarga) {
        this.prescripcionId = prescripcionId;
        this.prescriptor = prescriptor;
        this.inclusionPmm = inclusionPmm;
        this.lookupByTipoFrecPautaFk = lookupByTipoFrecPautaFk;
        this.via = via;
        this.dosis = dosis;
        this.posologia = posologia;
        this.tipoFormaFk = tipoFormaFk;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        this.fechaCarga = fechaCarga;
    }
    public Prescripcion(BigDecimal prescripcionId, Prescriptor prescriptor, InclusionPmm inclusionPmm, Lookup lookupByTipoPrescripcionFk, Lookup lookupByTipoFrecPautaFk, String via, BigDecimal dosis, String posologia, BigDecimal tipoFormaFk, Date fechaInicial, Date fechaFinal, String textoPrescripcion, Timestamp fechaCarga, String estado, BigDecimal tipoFk, String observaciones, Set recetas, Set ordenMedicas, Set inclusionPmms, Set citas, Set incidencias) {
       this.prescripcionId = prescripcionId;
       this.prescriptor = prescriptor;
       this.inclusionPmm = inclusionPmm;
       this.lookupByTipoPrescripcionFk = lookupByTipoPrescripcionFk;
       this.lookupByTipoFrecPautaFk = lookupByTipoFrecPautaFk;
       this.via = via;
       this.dosis = dosis;
       this.posologia = posologia;
       this.tipoFormaFk = tipoFormaFk;
       this.fechaInicial = fechaInicial;
       this.fechaFinal = fechaFinal;
       this.textoPrescripcion = textoPrescripcion;
       this.fechaCarga = fechaCarga;
       this.estado = estado;
       this.tipoFk = tipoFk;
       this.observaciones = observaciones;
       this.recetas = recetas;
       this.ordenMedicas = ordenMedicas;
       this.inclusionPmms = inclusionPmms;
       this.citas = citas;
       this.incidencias = incidencias;
    }
   
    public BigDecimal getPrescripcionId() {
        return this.prescripcionId;
    }
    
    public void setPrescripcionId(BigDecimal prescripcionId) {
        this.prescripcionId = prescripcionId;
    }
    public Prescriptor getPrescriptor() {
        return this.prescriptor;
    }
    
    public void setPrescriptor(Prescriptor prescriptor) {
        this.prescriptor = prescriptor;
    }
    public InclusionPmm getInclusionPmm() {
        return this.inclusionPmm;
    }
    
    public void setInclusionPmm(InclusionPmm inclusionPmm) {
        this.inclusionPmm = inclusionPmm;
    }
    public Lookup getLookupByTipoPrescripcionFk() {
        return this.lookupByTipoPrescripcionFk;
    }
    
    public void setLookupByTipoPrescripcionFk(Lookup lookupByTipoPrescripcionFk) {
        this.lookupByTipoPrescripcionFk = lookupByTipoPrescripcionFk;
    }
    public Lookup getLookupByTipoFrecPautaFk() {
        return this.lookupByTipoFrecPautaFk;
    }
    
    public void setLookupByTipoFrecPautaFk(Lookup lookupByTipoFrecPautaFk) {
        this.lookupByTipoFrecPautaFk = lookupByTipoFrecPautaFk;
    }
    public String getVia() {
        return this.via;
    }
    
    public void setVia(String via) {
        this.via = via;
    }
    public BigDecimal getDosis() {
        return this.dosis;
    }
    
    public void setDosis(BigDecimal dosis) {
        this.dosis = dosis;
    }
    public String getPosologia() {
        return this.posologia;
    }
    
    public void setPosologia(String posologia) {
        this.posologia = posologia;
    }
    public BigDecimal getTipoFormaFk() {
        return this.tipoFormaFk;
    }
    
    public void setTipoFormaFk(BigDecimal tipoFormaFk) {
        this.tipoFormaFk = tipoFormaFk;
    }
    public Date getFechaInicial() {
        return this.fechaInicial;
    }
    
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }
    public Date getFechaFinal() {
        return this.fechaFinal;
    }
    
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
    public String getTextoPrescripcion() {
        return this.textoPrescripcion;
    }
    
    public void setTextoPrescripcion(String textoPrescripcion) {
        this.textoPrescripcion = textoPrescripcion;
    }
    public Timestamp getFechaCarga() {
        return this.fechaCarga;
    }
    
    public void setFechaCarga(Timestamp fechaCarga) {
        this.fechaCarga = fechaCarga;
    }
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public BigDecimal getTipoFk() {
        return this.tipoFk;
    }
    
    public void setTipoFk(BigDecimal tipoFk) {
        this.tipoFk = tipoFk;
    }
    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public Set getRecetas() {
        return this.recetas;
    }
    
    public void setRecetas(Set recetas) {
        this.recetas = recetas;
    }
    public Set getOrdenMedicas() {
        return this.ordenMedicas;
    }
    
    public void setOrdenMedicas(Set ordenMedicas) {
        this.ordenMedicas = ordenMedicas;
    }
    public Set getInclusionPmms() {
        return this.inclusionPmms;
    }
    
    public void setInclusionPmms(Set inclusionPmms) {
        this.inclusionPmms = inclusionPmms;
    }
    public Set getCitas() {
        return this.citas;
    }
    
    public void setCitas(Set citas) {
        this.citas = citas;
    }
    public Set getIncidencias() {
        return this.incidencias;
    }
    
    public void setIncidencias(Set incidencias) {
        this.incidencias = incidencias;
    }




}


