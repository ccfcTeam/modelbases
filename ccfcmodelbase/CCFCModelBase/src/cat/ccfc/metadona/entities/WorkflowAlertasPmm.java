package cat.ccfc.metadona.entities;
// Generated Mar 10, 2015 7:14:34 PM by Hibernate Tools 4.3.1



/**
 * WorkflowAlertasPmm generated by hbm2java
 */
public class WorkflowAlertasPmm  implements java.io.Serializable {


     private WorkflowAlertasPmmId id;

    public WorkflowAlertasPmm() {
    }

    public WorkflowAlertasPmm(WorkflowAlertasPmmId id) {
       this.id = id;
    }
   
    public WorkflowAlertasPmmId getId() {
        return this.id;
    }
    
    public void setId(WorkflowAlertasPmmId id) {
        this.id = id;
    }




}


