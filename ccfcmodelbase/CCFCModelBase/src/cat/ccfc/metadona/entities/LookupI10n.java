package cat.ccfc.metadona.entities;
// Generated Jan 2, 2015 4:24:46 PM by Hibernate Tools 4.3.1



/**
 * LookupI10n generated by hbm2java
 */
public class LookupI10n  implements java.io.Serializable {


     private LookupI10nId id;
     private Lookup lookup;
     private String texto;

    public LookupI10n() {
    }

    public LookupI10n(LookupI10nId id, Lookup lookup, String texto) {
       this.id = id;
       this.lookup = lookup;
       this.texto = texto;
    }
   
    public LookupI10nId getId() {
        return this.id;
    }
    
    public void setId(LookupI10nId id) {
        this.id = id;
    }
    public Lookup getLookup() {
        return this.lookup;
    }
    
    public void setLookup(Lookup lookup) {
        this.lookup = lookup;
    }
    public String getTexto() {
        return this.texto;
    }
    
    public void setTexto(String texto) {
        this.texto = texto;
    }




}


