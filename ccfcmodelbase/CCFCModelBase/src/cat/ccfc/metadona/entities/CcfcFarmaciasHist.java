/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.ccfc.metadona.entities;


public class CcfcFarmaciasHist  implements java.io.Serializable {
    
    
    private CcfcFarmaciasHistId id;  
    private String fkColegio;
    private String razonSocial;
    private FarmaciaPmm farmaciaPmm;
    

    /**
     * @return the id
     */
    public CcfcFarmaciasHistId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(CcfcFarmaciasHistId id) {
        this.id = id;
    }

    /**
     * @return the fkColegio
     */
    public String getFkColegio() {
        return fkColegio;
    }

    /**
     * @param fkColegio the fkColegio to set
     */
    public void setFkColegio(String fkColegio) {
        this.fkColegio = fkColegio;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * @return the farmaciaPmm
     */
    public FarmaciaPmm getFarmaciaPmm() {
        return farmaciaPmm;
    }

    /**
     * @param farmaciaPmm the farmaciaPmm to set
     */
    public void setFarmaciaPmm(FarmaciaPmm farmaciaPmm) {
        this.farmaciaPmm = farmaciaPmm;
    }
    
    
}
