/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.ccfc.metadona.entities;

import java.util.Date;


public class CcfcFarmaciasHistId implements java.io.Serializable{
    
    private String idFarmaciaUp;
    private Date fechaAlta;

    /**
     * @return the idFarmaciaUp
     */
    public String getIdFarmaciaUp() {
        return idFarmaciaUp;
    }

    /**
     * @return the fechaAlta
     */
    public Date getFechaAlta() {
        return fechaAlta;
    }

    /**
     * @param idFarmaciaUp the idFarmaciaUp to set
     */
    public void setIdFarmaciaUp(String idFarmaciaUp) {
        this.idFarmaciaUp = idFarmaciaUp;
    }

    /**
     * @param fechaAlta the fechaAlta to set
     */
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
    
    
    
}
