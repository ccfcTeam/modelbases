package cat.ccfc.metadona.entities;
// Generated Jan 2, 2015 5:41:17 PM by Hibernate Tools 4.3.1



/**
 * CcfcPoblaciones generated by hbm2java
 */
public class CcfcPoblaciones  implements java.io.Serializable {


     private CcfcPoblacionesId id;
     private CcfcProvincias ccfcProvincias;
     private String nombrePoblacion;

    public CcfcPoblaciones() {
    }

	
    public CcfcPoblaciones(CcfcPoblacionesId id, CcfcProvincias ccfcProvincias) {
        this.id = id;
        this.ccfcProvincias = ccfcProvincias;
    }
    public CcfcPoblaciones(CcfcPoblacionesId id, CcfcProvincias ccfcProvincias, String nombrePoblacion) {
       this.id = id;
       this.ccfcProvincias = ccfcProvincias;
       this.nombrePoblacion = nombrePoblacion;
    }
   
    public CcfcPoblacionesId getId() {
        return this.id;
    }
    
    public void setId(CcfcPoblacionesId id) {
        this.id = id;
    }
    public CcfcProvincias getCcfcProvincias() {
        return this.ccfcProvincias;
    }
    
    public void setCcfcProvincias(CcfcProvincias ccfcProvincias) {
        this.ccfcProvincias = ccfcProvincias;
    }
    public String getNombrePoblacion() {
        return this.nombrePoblacion;
    }
    
    public void setNombrePoblacion(String nombrePoblacion) {
        this.nombrePoblacion = nombrePoblacion;
    }




}


