package cat.ccfc.metadona.dao;

import cat.ccfc.metadona.entities.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
public class TestDao {

    /*
    Classname LookupDao
    *
    Version info 1.0
    *
    Copyright notice
    */

    public List getAllRowsLookup() throws Exception {
            try {
                System.out.println ("1==================>");
                    Session session = getSessionFactory().openSession();
                    Transaction transaction = null;
                    transaction = session.beginTransaction();
                    // Note: Is the class name, not the table name
                    List result = session.createQuery("from InclusionPmm").list();
                    session.getTransaction().commit();
                System.out.println ("2==================>");
                    session.close();
                    return result;
            } 
            catch (HibernateException e) {
                    throw e;
            }
            catch (Exception e) {
                    throw e;
            }
    }


    private static SessionFactory sessionFactory;
     
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // loads configuration and mappings
            Configuration configuration = new Configuration().configure();
            ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
             
            // builds a session factory from the service registry
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);           
        }
         
        return sessionFactory;
    }    
    
    public static void main (String srgs []){
        try {
        System.out.println ("=================Size:=>"+new TestDao ().getAllRowsLookup().size());
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
