package cat.ccfc.sifare.operations.dao;


import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
public class TestDao {

    


    private static SessionFactory sessionFactory;
     
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // loads configuration and mappings
            Configuration configuration = new Configuration().configure();
            ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
             
            // builds a session factory from the service registry
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);           
        }
         
        return sessionFactory;
    }    
    
    public static void main (String srgs []){
        try {
     
                System.out.println("-----------------STARTING-----------------------");
          Session session = getSessionFactory().openSession();
        //  Query query=session.createQuery("from CcfcColegios order by 1 ");
        //  query.setMaxResults(1);
          
         // CcfcColegios ccfcColegios=(CcfcColegios)query.uniqueResult();
         // System.out.println(ccfcColegios.getDescColegio());
         
          System.out.println("-----------------START-----------------------");
          TestDao testDao=new TestDao();
          //testDao.getDataTest();
          //testDao.getCcfcFarmacialoHist("F98000089");
         //testDao.getCcfcFarmacialoHist("testtttt1");
          
          System.out.println("-----------------END-----------------------");

        } catch (Exception ex){
                System.out.println("-----------------EXCEPTION-----------------------");
            ex.printStackTrace();
        }
    }

 
}
