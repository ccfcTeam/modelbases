/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operationsmodelbase;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import cat.ccfc.sifare.operations.entities.*;
/**
 *
 * @author gyedla
 */
public class OperationsModelBase {

    /**
     * @param args the command line arguments
     */
    
    public static void main (String srgs []){
        try {
     
          Session session = getSessionFactory().openSession();
        //  Query query=session.createQuery("from CcfcColegios order by 1 ");
        //  query.setMaxResults(1);
          
         // CcfcColegios ccfcColegios=(CcfcColegios)query.uniqueResult();
         // System.out.println(ccfcColegios.getDescColegio());
         
          
          OperationsModelBase testDao=new OperationsModelBase();
          
          //testDao.getCcfcFarmaciolaHist();
         // testDao.getCcfcFarmacialoHist("testtttt1");
          
          System.out.println("----------------------------------------");
       /*           
          testDao.getCcfcColegios();
          testDao.getCcfcRegionesSanitarias();
          testDao.getCcfcSectorsSanitaris();
          testDao.getCcfcAreesBasiquesSalut();
          testDao.getCcfcProvincias();
          testDao.getCcfcComarcas();
          testDao.getCcfcMunicipios();
          
          
          CcfcFarmaciolaHist farmacialoHist=new CcfcFarmaciolaHist(); 
          
          testDao.getCcfcPoblacionesIne();
          testDao.getCcfcServiciosProfesionales(); */
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }



    private static SessionFactory sessionFactory;
     
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // loads configuration and mappings
            sessionFactory = new Configuration().configure("operations_hibernate.cfg.xml").buildSessionFactory();

        }
         
        return sessionFactory;
    }    
    
}
