package cat.ccfc.sifare.dao;


import cat.ccfc.sifare.entities.CcfcAreesBasiquesSalut;
import cat.ccfc.sifare.entities.CcfcColegios;
import cat.ccfc.sifare.entities.CcfcComarcas;
import cat.ccfc.sifare.entities.CcfcFarmaciasHist;
import cat.ccfc.sifare.entities.CcfcFarmaciolaHist;
import cat.ccfc.sifare.entities.CcfcMunicipios;
import cat.ccfc.sifare.entities.CcfcPoblacionesIne;
import cat.ccfc.sifare.entities.CcfcProvincias;
import cat.ccfc.sifare.entities.CcfcRegionesSanitarias;
import cat.ccfc.sifare.entities.CcfcSectorsSanitaris;
import cat.ccfc.sifare.entities.CcfcServiciosProfesionales;
import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
public class TestDao {

    


    private static SessionFactory sessionFactory;
     
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // loads configuration and mappings
            Configuration configuration = new Configuration().configure();
            ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
             
            // builds a session factory from the service registry
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);           
        }
         
        return sessionFactory;
    }    
    
    public static void main (String srgs []){
        try {
     
                System.out.println("-----------------STARTING-----------------------");
          Session session = getSessionFactory().openSession();
        //  Query query=session.createQuery("from CcfcColegios order by 1 ");
        //  query.setMaxResults(1);
          
         // CcfcColegios ccfcColegios=(CcfcColegios)query.uniqueResult();
         // System.out.println(ccfcColegios.getDescColegio());
         
          System.out.println("-----------------START-----------------------");
          TestDao testDao=new TestDao();
          //testDao.getDataTest();
          testDao.getCcfcFarmacialoHist("F98000089");
         //testDao.getCcfcFarmacialoHist("testtttt1");
          
          System.out.println("-----------------END-----------------------");
       /*           
          testDao.getCcfcColegios();
          testDao.getCcfcRegionesSanitarias();
          testDao.getCcfcSectorsSanitaris();
          testDao.getCcfcAreesBasiquesSalut();
          testDao.getCcfcProvincias();
          testDao.getCcfcComarcas();
          testDao.getCcfcMunicipios();
          
          
          CcfcFarmaciolaHist farmacialoHist=new CcfcFarmaciolaHist(); 
          
          testDao.getCcfcPoblacionesIne();
          testDao.getCcfcServiciosProfesionales(); */
        } catch (Exception ex){
                System.out.println("-----------------EXCEPTION-----------------------");
            ex.printStackTrace();
        }
    }

public static CcfcFarmaciolaHist getCcfcFarmacialoHist111(String idFarmaciola) throws Exception {
        Session session = getSessionFactory().openSession();
        try
        {
         System.out.println("idFarmaciola : "+idFarmaciola);
         Query query=session.createQuery("from CcfcFarmaciolaHist where id.idFarmaciola= :BindIdFarmaciola");
         query.setParameter("BindIdFarmaciola", idFarmaciola);
         CcfcFarmaciolaHist farmacialoHist = (CcfcFarmaciolaHist)query.uniqueResult();
            System.out.println("Got farm...");
            if(farmacialoHist!=null)  {
            farmacialoHist.getId();
            
            }else{
                System.out.println("Farmacia is null!!");
            }
            System.out.println("After Id ...");
         return farmacialoHist;
        }
        catch (HibernateException e)
        {
          throw new HibernateException(e);
        }
        catch (Exception e)
        {
          throw new Exception(e);
        } finally {
          session.close();
        } 
    }  

    
    
    private static final String test_sql="select ra.ERROR ||' - '|| ra.DESCRIPCION ERRORES from ccfc_registro_acceso_hist_err ra\n" +
        "where ID = :BindId";
     
    public List getDataTest()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createSQLQuery(test_sql);
      query.setParameter("BindId", "1642186966");
      List<String> result = query.list();
      for(String obj: result){
          System.out.println("-------"+obj);
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }  
    
    
    public static CcfcFarmaciolaHist getCcfcFarmacialoHist(String idFarmaciola) throws Exception {
        Session session = getSessionFactory().openSession();
        try
        {
         System.out.println("idFarmaciola : "+idFarmaciola);
         Query query=session.createQuery("from CcfcFarmaciolaHist where id.idFarmaciola= :BindIdFarmaciola");
         query.setParameter("BindIdFarmaciola", idFarmaciola);
         CcfcFarmaciolaHist farmacialoHist = (CcfcFarmaciolaHist)query.uniqueResult();
            System.out.println("Got farm...");
            if(farmacialoHist!=null)  {
            farmacialoHist.getId();
            
            }else{
                System.out.println("Farmacia is nulllllllllllllllllll");
            }
            System.out.println("After Id ...");
         return farmacialoHist;
        }
        catch (HibernateException e)
        {
          throw new HibernateException(e);
        }
        catch (Exception e)
        {
          throw new Exception(e);
        } finally {
          session.close();
        } 
    }    
  public List getCcfcFarmaciolaHist()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcFarmaciolaHist where id.idFarmaciola='F98000090' ");
System.err.println ("****************************>>>>>><<<<<<<<<<<<");
        
      List<CcfcFarmaciolaHist> result = query.list();
System.err.println ("****************************>>>>>>"+result.size());
      for(CcfcFarmaciolaHist obj: result){
          System.err.println("================>"+obj.getId());
          System.err.println("================>"+obj.getId().getIdFarmaciaUp());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
   
   public List getCcfcFarmaciaHist()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcFarmaciasHist");
System.err.println ("****************************>>>>>><<<<<<<<<<<<");
        
      List<CcfcFarmaciasHist> result = query.list();
System.err.println ("****************************>>>>>>"+result.size());
      for(CcfcFarmaciasHist obj: result){
          System.err.println("================>"+obj.getId().getIdFarmaciaUp());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
   
  public List getCcfcColegios()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcColegios");
        
      List<CcfcColegios> result = query.list();

      for(CcfcColegios obj: result){
          System.out.println(obj.getIdColegio()+" : "+obj.getDescColegio());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }    
 
  
  
  public List getCcfcRegionesSanitarias()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcRegionesSanitarias order by idRegion");
        
      List<CcfcRegionesSanitarias> result = query.list();
      for(CcfcRegionesSanitarias obj: result){
          System.out.println(obj.getIdRegion()+" : "+obj.getDescripcion());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
  
  
  public List getCcfcSectorsSanitaris()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcSectorsSanitaris where fkRegioSanitaria = :BindIdRegion");
      query.setParameter("BindIdRegion", "61");
      List<CcfcSectorsSanitaris> result = query.list();
      for(CcfcSectorsSanitaris obj: result){
          System.out.println(obj.getIdSectorSanitari()+" : "+obj.getDescripcio());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }  
  
  public List getCcfcAreesBasiquesSalut()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcAreesBasiquesSalut where fkRegioSanitaria = :BindIdRegion and fkSectorSanitari = :BindIdSectorSanitari");
      query.setParameter("BindIdRegion", "61");
      query.setParameter("BindIdSectorSanitari", "6101");
      List<CcfcAreesBasiquesSalut> result = query.list();
      for(CcfcAreesBasiquesSalut obj: result){
          System.out.println(" getCcfcAreesBasiquesSalut:"+obj.getIdAreaBasicaSalut()+" : "+obj.getDescripcio());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
  
  
  public List getCcfcProvincias()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcProvincias order by nombreProvincia");
        
      List<CcfcProvincias> result = query.list();

      for(CcfcProvincias obj: result){
          System.out.println(" getCcfcProvincias : " +obj.getId().getIdProvincia()+" : "+obj.getNombreProvincia());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }   

  
  public List getCcfcComarcas()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcComarcas order by nombreComarca");
        
      List<CcfcComarcas> result = query.list();

      for(CcfcComarcas obj: result){
          System.out.println(" getCcfcComarcas : "+obj.getId().getIdComarcaIne()+" : "+obj.getNombreComarca());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
  
  
  public List getCcfcMunicipios()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcMunicipios where id.idProvincia = :BindIdProvincia and id.idComarcaIne = :BindIdComarcaIne order by nombreMunicipio");
      query.setParameter("BindIdProvincia", "25");
      query.setParameter("BindIdComarcaIne", "15");
      List<CcfcMunicipios> result = query.list();
      for(CcfcMunicipios obj: result){
          System.out.println(" getCcfcMunicipios:"+obj.getNombreMunicipio()+" : "+obj.getId().getIdComarcaIne()+" : "+obj.getId().getIdProvincia());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  } 

  
    public List getCcfcPoblacionesIne()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = 
              session.createQuery("from CcfcPoblacionesIne where idProvincia = :BindIdProvincia and idComarcaIne = :BindIdComarcaIne and id.idMunicipioIne = :BindIdMunicipioIne order by nombrePoblacion");
      query.setParameter("BindIdProvincia", "17");
      query.setParameter("BindIdComarcaIne", "15");
      query.setParameter("BindIdMunicipioIne", "170242");
      List<CcfcPoblacionesIne> result = query.list();
      for(CcfcPoblacionesIne obj: result){
          System.out.println(" getCcfcPoblacionesIne:"+obj.getNombrePoblacion()+" : "+obj.getId().getIdPoblacionIne()+" : "+obj.getId().getIdMunicipioIne());
      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }
    
    
  public List getCcfcServiciosProfesionales()
    throws Exception
  {
      Session session = getSessionFactory().openSession();
      try
      {
      Query query = session.createQuery("from CcfcServiciosProfesionales where id.esActivo = 'S' and id.esAppPortal = 'N' order by id.descripcionServicio");

      List<CcfcServiciosProfesionales> result = query.list();
//      for(CcfcServiciosProfesionales obj: result){
//          System.out.println(" getCcfcServiciosProfesionales:"+obj.getId().getIdServicioProfesional()+" : "+obj.getId().getEsActivo());
//      }
      return result;
    }
    catch (HibernateException e)
    {
      throw e;
    }finally
      {
        session.close();
      }
  }    
  
}
