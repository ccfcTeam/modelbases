package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcHistoricPwdId generated by hbm2java
 */
public class CcfcHistoricPwdId  implements java.io.Serializable {


     private String idUsuario;
     private String password;

    public CcfcHistoricPwdId() {
    }

    public CcfcHistoricPwdId(String idUsuario, String password) {
       this.idUsuario = idUsuario;
       this.password = password;
    }
   
    public String getIdUsuario() {
        return this.idUsuario;
    }
    
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CcfcHistoricPwdId) ) return false;
		 CcfcHistoricPwdId castOther = ( CcfcHistoricPwdId ) other; 
         
		 return ( (this.getIdUsuario()==castOther.getIdUsuario()) || ( this.getIdUsuario()!=null && castOther.getIdUsuario()!=null && this.getIdUsuario().equals(castOther.getIdUsuario()) ) )
 && ( (this.getPassword()==castOther.getPassword()) || ( this.getPassword()!=null && castOther.getPassword()!=null && this.getPassword().equals(castOther.getPassword()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdUsuario() == null ? 0 : this.getIdUsuario().hashCode() );
         result = 37 * result + ( getPassword() == null ? 0 : this.getPassword().hashCode() );
         return result;
   }   


}


