package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcDispensaciones generated by hbm2java
 */
public class CcfcDispensaciones  implements java.io.Serializable {


     private CcfcDispensacionesId id;

    public CcfcDispensaciones() {
    }

    public CcfcDispensaciones(CcfcDispensacionesId id) {
       this.id = id;
    }
   
    public CcfcDispensacionesId getId() {
        return this.id;
    }
    
    public void setId(CcfcDispensacionesId id) {
        this.id = id;
    }




}


