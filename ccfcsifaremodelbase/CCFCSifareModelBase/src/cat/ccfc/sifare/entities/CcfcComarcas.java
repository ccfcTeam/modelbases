package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcComarcas generated by hbm2java
 */
public class CcfcComarcas  implements java.io.Serializable {


     private CcfcComarcasId id;
     private String nombreComarca;

    public CcfcComarcas() {
    }

	
    public CcfcComarcas(CcfcComarcasId id) {
        this.id = id;
    }
    public CcfcComarcas(CcfcComarcasId id, String nombreComarca) {
       this.id = id;
       this.nombreComarca = nombreComarca;
    }
   
    public CcfcComarcasId getId() {
        return this.id;
    }
    
    public void setId(CcfcComarcasId id) {
        this.id = id;
    }
    public String getNombreComarca() {
        return this.nombreComarca;
    }
    
    public void setNombreComarca(String nombreComarca) {
        this.nombreComarca = nombreComarca;
    }




}


