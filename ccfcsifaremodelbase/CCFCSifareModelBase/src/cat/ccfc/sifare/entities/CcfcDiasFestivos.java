package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcDiasFestivos generated by hbm2java
 */
public class CcfcDiasFestivos  implements java.io.Serializable {


     private CcfcDiasFestivosId id;

    public CcfcDiasFestivos() {
    }

    public CcfcDiasFestivos(CcfcDiasFestivosId id) {
       this.id = id;
    }
   
    public CcfcDiasFestivosId getId() {
        return this.id;
    }
    
    public void setId(CcfcDiasFestivosId id) {
        this.id = id;
    }




}


