package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcPermisos generated by hbm2java
 */
public class CcfcPermisos  implements java.io.Serializable {


     private CcfcPermisosId id;

    public CcfcPermisos() {
    }

    public CcfcPermisos(CcfcPermisosId id) {
       this.id = id;
    }
   
    public CcfcPermisosId getId() {
        return this.id;
    }
    
    public void setId(CcfcPermisosId id) {
        this.id = id;
    }




}


