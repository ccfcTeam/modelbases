package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcConfiguracion generated by hbm2java
 */
public class CcfcConfiguracion  implements java.io.Serializable {


     private CcfcConfiguracionId id;
     private String valor;
     private String descripcion;

    public CcfcConfiguracion() {
    }

	
    public CcfcConfiguracion(CcfcConfiguracionId id) {
        this.id = id;
    }
    public CcfcConfiguracion(CcfcConfiguracionId id, String valor, String descripcion) {
       this.id = id;
       this.valor = valor;
       this.descripcion = descripcion;
    }
   
    public CcfcConfiguracionId getId() {
        return this.id;
    }
    
    public void setId(CcfcConfiguracionId id) {
        this.id = id;
    }
    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }




}


