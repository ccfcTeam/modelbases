package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;

/**
 * CcfcIncidenciasLopd generated by hbm2java
 */
public class CcfcIncidenciasLopd  implements java.io.Serializable {


     private BigDecimal idIncidencia;
     private String fkUsuario;
     private String descripcionBreve;
     private String fkTipoIncidencia;
     private String fkGraveIncidencia;
     private Date fechaInicio;
     private Date fechaCierre;
     private String notificante;
     private String notificado;
     private String afectacionDatos;
     private String responsableDatos;
     private String efectosProducidos;
     private String medidasTomadas;
     private String accionesPreventivas;
     private String datosAfectados;
     private String medidasRecuperacion;
     private String datosManuales;
     private String autorizacion;
     private Date fechaAlta;

    public CcfcIncidenciasLopd() {
    }

	
    public CcfcIncidenciasLopd(BigDecimal idIncidencia) {
        this.idIncidencia = idIncidencia;
    }
    public CcfcIncidenciasLopd(BigDecimal idIncidencia, String fkUsuario, String descripcionBreve, String fkTipoIncidencia, String fkGraveIncidencia, Date fechaInicio, Date fechaCierre, String notificante, String notificado, String afectacionDatos, String responsableDatos, String efectosProducidos, String medidasTomadas, String accionesPreventivas, String datosAfectados, String medidasRecuperacion, String datosManuales, String autorizacion, Date fechaAlta) {
       this.idIncidencia = idIncidencia;
       this.fkUsuario = fkUsuario;
       this.descripcionBreve = descripcionBreve;
       this.fkTipoIncidencia = fkTipoIncidencia;
       this.fkGraveIncidencia = fkGraveIncidencia;
       this.fechaInicio = fechaInicio;
       this.fechaCierre = fechaCierre;
       this.notificante = notificante;
       this.notificado = notificado;
       this.afectacionDatos = afectacionDatos;
       this.responsableDatos = responsableDatos;
       this.efectosProducidos = efectosProducidos;
       this.medidasTomadas = medidasTomadas;
       this.accionesPreventivas = accionesPreventivas;
       this.datosAfectados = datosAfectados;
       this.medidasRecuperacion = medidasRecuperacion;
       this.datosManuales = datosManuales;
       this.autorizacion = autorizacion;
       this.fechaAlta = fechaAlta;
    }
   
    public BigDecimal getIdIncidencia() {
        return this.idIncidencia;
    }
    
    public void setIdIncidencia(BigDecimal idIncidencia) {
        this.idIncidencia = idIncidencia;
    }
    public String getFkUsuario() {
        return this.fkUsuario;
    }
    
    public void setFkUsuario(String fkUsuario) {
        this.fkUsuario = fkUsuario;
    }
    public String getDescripcionBreve() {
        return this.descripcionBreve;
    }
    
    public void setDescripcionBreve(String descripcionBreve) {
        this.descripcionBreve = descripcionBreve;
    }
    public String getFkTipoIncidencia() {
        return this.fkTipoIncidencia;
    }
    
    public void setFkTipoIncidencia(String fkTipoIncidencia) {
        this.fkTipoIncidencia = fkTipoIncidencia;
    }
    public String getFkGraveIncidencia() {
        return this.fkGraveIncidencia;
    }
    
    public void setFkGraveIncidencia(String fkGraveIncidencia) {
        this.fkGraveIncidencia = fkGraveIncidencia;
    }
    public Date getFechaInicio() {
        return this.fechaInicio;
    }
    
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public Date getFechaCierre() {
        return this.fechaCierre;
    }
    
    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }
    public String getNotificante() {
        return this.notificante;
    }
    
    public void setNotificante(String notificante) {
        this.notificante = notificante;
    }
    public String getNotificado() {
        return this.notificado;
    }
    
    public void setNotificado(String notificado) {
        this.notificado = notificado;
    }
    public String getAfectacionDatos() {
        return this.afectacionDatos;
    }
    
    public void setAfectacionDatos(String afectacionDatos) {
        this.afectacionDatos = afectacionDatos;
    }
    public String getResponsableDatos() {
        return this.responsableDatos;
    }
    
    public void setResponsableDatos(String responsableDatos) {
        this.responsableDatos = responsableDatos;
    }
    public String getEfectosProducidos() {
        return this.efectosProducidos;
    }
    
    public void setEfectosProducidos(String efectosProducidos) {
        this.efectosProducidos = efectosProducidos;
    }
    public String getMedidasTomadas() {
        return this.medidasTomadas;
    }
    
    public void setMedidasTomadas(String medidasTomadas) {
        this.medidasTomadas = medidasTomadas;
    }
    public String getAccionesPreventivas() {
        return this.accionesPreventivas;
    }
    
    public void setAccionesPreventivas(String accionesPreventivas) {
        this.accionesPreventivas = accionesPreventivas;
    }
    public String getDatosAfectados() {
        return this.datosAfectados;
    }
    
    public void setDatosAfectados(String datosAfectados) {
        this.datosAfectados = datosAfectados;
    }
    public String getMedidasRecuperacion() {
        return this.medidasRecuperacion;
    }
    
    public void setMedidasRecuperacion(String medidasRecuperacion) {
        this.medidasRecuperacion = medidasRecuperacion;
    }
    public String getDatosManuales() {
        return this.datosManuales;
    }
    
    public void setDatosManuales(String datosManuales) {
        this.datosManuales = datosManuales;
    }
    public String getAutorizacion() {
        return this.autorizacion;
    }
    
    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }
    public Date getFechaAlta() {
        return this.fechaAlta;
    }
    
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }




}


