package cat.ccfc.sifare.entities;
// Generated May 20, 2015 2:48:08 PM by Hibernate Tools 4.3.1



/**
 * CcfcPermisosId generated by hbm2java
 */
public class CcfcPermisosId  implements java.io.Serializable {


     private String idPermis;
     private String descPermis01;
     private String descPermis02;

    public CcfcPermisosId() {
    }

    public CcfcPermisosId(String idPermis, String descPermis01, String descPermis02) {
       this.idPermis = idPermis;
       this.descPermis01 = descPermis01;
       this.descPermis02 = descPermis02;
    }
   
    public String getIdPermis() {
        return this.idPermis;
    }
    
    public void setIdPermis(String idPermis) {
        this.idPermis = idPermis;
    }
    public String getDescPermis01() {
        return this.descPermis01;
    }
    
    public void setDescPermis01(String descPermis01) {
        this.descPermis01 = descPermis01;
    }
    public String getDescPermis02() {
        return this.descPermis02;
    }
    
    public void setDescPermis02(String descPermis02) {
        this.descPermis02 = descPermis02;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CcfcPermisosId) ) return false;
		 CcfcPermisosId castOther = ( CcfcPermisosId ) other; 
         
		 return ( (this.getIdPermis()==castOther.getIdPermis()) || ( this.getIdPermis()!=null && castOther.getIdPermis()!=null && this.getIdPermis().equals(castOther.getIdPermis()) ) )
 && ( (this.getDescPermis01()==castOther.getDescPermis01()) || ( this.getDescPermis01()!=null && castOther.getDescPermis01()!=null && this.getDescPermis01().equals(castOther.getDescPermis01()) ) )
 && ( (this.getDescPermis02()==castOther.getDescPermis02()) || ( this.getDescPermis02()!=null && castOther.getDescPermis02()!=null && this.getDescPermis02().equals(castOther.getDescPermis02()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdPermis() == null ? 0 : this.getIdPermis().hashCode() );
         result = 37 * result + ( getDescPermis01() == null ? 0 : this.getDescPermis01().hashCode() );
         result = 37 * result + ( getDescPermis02() == null ? 0 : this.getDescPermis02().hashCode() );
         return result;
   }   


}


